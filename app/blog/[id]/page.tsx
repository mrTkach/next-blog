import { Metadata } from "next";

type PostProps = {
  params: {
    id: string;
  };
};

const BASE_URL = "https://jsonplaceholder.typicode.com/";

const getData = async (id: string) => {
  const response = await fetch(BASE_URL + "posts/" + id, {
    next: {
      revalidate: 60, // caching data for 60 sec
    },
  });
  return response.json();
};

export const generateMetadata = async ({
  params: { id },
}: PostProps): Promise<Metadata> => {
  const post = await getData(id);

  return {
    title: post.title,
  };
};

const Post = async ({ params: { id } }: PostProps) => {
  const post = await getData(id);

  return (
    <>
      <h1>{post.title}</h1>
      <article>{post.body}</article>
    </>
  );
};

export default Post;
