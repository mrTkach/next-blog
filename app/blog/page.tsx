import { Metadata } from "next";
import Link from "next/link";

const BASE_URL = "https://jsonplaceholder.typicode.com/";

const getData = async () => {
  const response = await fetch(BASE_URL + "posts", {
    next: {
      // caching data for 60 sec
      revalidate: 60,
    },
  });

  if (!response.ok) throw new Error("unable to fetch data");

  return response.json();
};

export const metadata: Metadata = {
  title: "Blog | Next",
};

const Blog = async () => {
  const posts: string[] = await getData();
  return (
    <>
      <h1>blog page</h1>
      <ul>
        {posts.map((post: any) => (
          <li key={post.id}>
            <Link href={`/blog/${post.id}`}>{post.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
};

export default Blog;
